package eu.theracraft.sellwand.commands;

import eu.theracraft.sellwand.SellWand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class CommandGivewand implements CommandExecutor {

    private SellWand plugin;

    public CommandGivewand(SellWand plugin) {
        this.plugin = plugin;
        Bukkit.getPluginCommand("givewand").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("sellwand.give")) {
            sender.sendMessage("§cYou don't have permission to use that!");
            return true;
        }

        if (args.length == 0) {
            if (sender instanceof Player) {
                // Give wand
                Player player = (Player) sender;
                Inventory inventory = player.getInventory();
                if (inventory.firstEmpty() == -1) sender.sendMessage("§cYour inventory is full!");
                else {
                    inventory.setItem(inventory.firstEmpty(), plugin.getSellWand());
                    sender.sendMessage("§eYou have been given a SellWand!");
                }
            } else {
                sender.sendMessage("§cTo give SellWands from the console please use the syntax:");
                sender.sendMessage("§c/" + label + " <player>");
            }
        } else if (args.length == 1) {
            if (sender.hasPermission("sellwand.give.others")) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target == null) {
                    sender.sendMessage("§cInvalid player!");
                } else {
                    Inventory inventory = target.getInventory();
                    if (inventory.firstEmpty() == -1) sender.sendMessage("§c" + target.getName() + "'s inventory is full!");
                    else {
                        inventory.setItem(inventory.firstEmpty(), plugin.getSellWand());
                        sender.sendMessage("§eYou have given " + target.getName() + " a SellWand!");
                        target.sendMessage("§eYou have been given a SellWand by " + sender.getName() + "!");
                    }
                }
            } else {
                sender.sendMessage("§cInvalid command syntax!");
                sender.sendMessage("§cUsage: /" + label);
            }
        } else {
            if (sender.hasPermission("sellwand.give.others")) {
                sender.sendMessage("§cInvalid command syntax!");
                sender.sendMessage("§cUsage: /" + label + " OR /" + label + " <player>");
            } else {
                sender.sendMessage("§cInvalid command syntax!");
                sender.sendMessage("§cUsage: /" + label);
            }
        }
        return true;
    }
}
