package eu.theracraft.sellwand;

import com.griefcraft.lwc.LWC;
import com.griefcraft.lwc.LWCPlugin;
import eu.theracraft.sellwand.commands.CommandGivewand;
import eu.theracraft.sellwand.listeners.ListenerPlayerInteract;
import net.coreprotect.CoreProtect;
import net.coreprotect.CoreProtectAPI;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class SellWand extends JavaPlugin {

    private Economy econ = null;
    private LWC lwc = null;
    private CoreProtectAPI cpAPI = null;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        if (!setupEconomy() ) {
            getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        LWCPlugin lwcPlugin = (LWCPlugin) Bukkit.getPluginManager().getPlugin("LWC");
        if (lwcPlugin != null) lwc = lwcPlugin.getLWC();

        CoreProtect cp = (CoreProtect) Bukkit.getPluginManager().getPlugin("CoreProtect");
        if (cp != null) cpAPI = cp.getAPI();

        new CommandGivewand(this);
        new ListenerPlayerInteract(this);
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public Economy getEconomy() {
        return econ;
    }

    public ItemStack getSellWand() {
        ItemStack stack = new ItemStack(Material.BLAZE_ROD, 1);
        stack.addUnsafeEnchantment(Enchantment.DURABILITY, 1);
        ItemMeta meta = stack.getItemMeta();
        if (meta != null) {
            meta.setUnbreakable(true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            meta.setDisplayName("§e§lSell§6§lWand");
            stack.setItemMeta(meta);
        }
        return stack;
    }

    public boolean isSellWand(ItemStack stack) {
        if (stack == null) return false;
        if (stack.getItemMeta() == null) return false;
        if (!stack.getType().equals(Material.BLAZE_ROD)) return false;
        if (stack.getEnchantmentLevel(Enchantment.DURABILITY) != 1) return false;
        if (!stack.getItemMeta().isUnbreakable()) return false;
        if (!stack.getItemMeta().getItemFlags().contains(ItemFlag.HIDE_ENCHANTS)) return false;
        if (!stack.getItemMeta().getDisplayName().equals("§e§lSell§6§lWand")) return false;
        return true;
    }

    public boolean hasLWC() {
        return lwc != null;
    }

    public LWC getLWC() {
        return lwc;
    }

    public boolean hasCoreProtectAPI() {
        return cpAPI != null;
    }

    public CoreProtectAPI getCoreProtectAPI() {
        return cpAPI;
    }
}
