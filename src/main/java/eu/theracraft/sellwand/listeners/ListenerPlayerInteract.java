package eu.theracraft.sellwand.listeners;

import com.griefcraft.model.Protection;
import eu.theracraft.sellwand.SellWand;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.bukkit.Bukkit.getLogger;

public class ListenerPlayerInteract implements Listener {

    private SellWand plugin;
    Map<Material, Double> itemPrices = new HashMap<>();

    public ListenerPlayerInteract(SellWand plugin) {
        this.plugin = plugin;
        setup();
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public void setup() {
        for (String item : plugin.getConfig().getStringList("items")) {
            String[] part = item.split(":");
            Material mat = Material.matchMaterial(part[0]);
            if (mat == null) {
                getLogger().warning("Invalid material \"" + part[0] + "\"");
                continue;
            }
            double price;
            try {
                price = Double.parseDouble(part[1]);
            } catch (NumberFormatException e) {
                getLogger().warning("Unable to parse \"" + part[1] + "\" into a double");
                continue;
            }
            itemPrices.put(mat, price);
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
        Block block = event.getClickedBlock();
        if (block == null) return;
        if (!block.getType().equals(Material.CHEST) && !block.getType().equals(Material.TRAPPED_CHEST)) return;
        if (!(block.getState() instanceof Chest)) return;
        if (!event.hasItem()) return;
        if (!plugin.isSellWand(event.getItem())) return;
        Player player = event.getPlayer();

        event.setCancelled(true);

        if (!player.hasPermission("sellwand.use")) {
            player.sendMessage("§cYou don't have permission to use that!");
            return;
        }

        if (plugin.hasLWC()) {
            Protection protection = plugin.getLWC().findProtection(block);
            if (protection == null) {
                player.sendMessage("§cPlease lock the chest before selling from it!");
                return;
            }
            if (!plugin.getLWC().canAccessProtection(player, protection)) return;
        }

        Map<Material, Double> itemPricesCopy = new HashMap<>(itemPrices);
        Inventory inventory =  ((Chest) block.getState()).getInventory();
        double priceTotal = 0;
        int totalItemAmount = 0;
        for (ItemStack stack : inventory.getContents()) {
            if (stack == null) continue;
            Material type = stack.getType();
            if (!itemPricesCopy.containsKey(type)) continue;
            double pricePerItem = itemPricesCopy.get(type);
            int totalItems = 0;

            Map<Integer, ? extends ItemStack> allItems = inventory.all(type);
            List<Integer> keySet = new ArrayList<>(allItems.keySet());
            for (Integer integer : keySet) {
                totalItems += allItems.get(integer).getAmount();
                if (plugin.hasCoreProtectAPI())
                    plugin.getCoreProtectAPI().logContainerTransaction(player.getName(), block.getLocation());
                inventory.clear(integer);
            }
            itemPricesCopy.remove(type);
            priceTotal += pricePerItem * totalItems;
            totalItemAmount += totalItems;
        }

        if (priceTotal == 0 || totalItemAmount == 0) {
            player.sendMessage("§cNo items in this chest can be sold.");
            return;
        }

        DecimalFormat formatter = new DecimalFormat("#,###");
        if (!plugin.getEconomy().depositPlayer(player, priceTotal).transactionSuccess()) {
            Bukkit.dispatchCommand(player,
                    "mail send Johanmans10 [SellWand] I was supposed to get $" + formatter.format(priceTotal) + " but an error occurred.");
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
                    "mail send Johanmans10 [&e&lSell&6&lWand] " + player + ": $" + formatter.format(priceTotal));
            return;
        }

        player.sendMessage("§aYou sold " + formatter.format(totalItemAmount) + " item(s) for a total of $" + formatter.format(priceTotal) + ".");
    }

}
